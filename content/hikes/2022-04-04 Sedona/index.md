---
title: "Sedona"
date: 2022-04-04T00:00:00+00:00
images: ["images/IMG_7109.jpg"]
locations: "Sedona"
seasons: ["Spring"]
---

Hike to a cave in Sedona!
