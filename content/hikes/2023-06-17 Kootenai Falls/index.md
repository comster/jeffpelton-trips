---
title: "Kootenai Falls"
date: 2023-06-17T00:00:00+00:00
images: ["images/IMG_1278.JPG"]
locations: "Kootenai Falls"
seasons: ["Spring"]
---

Quick hike around Kootenai Falls!
