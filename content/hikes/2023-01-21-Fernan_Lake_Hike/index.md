---
title: "Fernan Lake"
date: 2023-01-21T00:00:00+00:00
images: ["images/IMG_0413.jpg"]
locations: "Fernan Lake"
seasons: ["Winter"]
---

Quick evening hike of Fernan Lake!
