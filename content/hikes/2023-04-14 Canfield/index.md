---
title: "Canfield Mountain"
date: 2023-04-14T00:00:00+00:00
images: ["images/IMG_0889.jpg"]
locations: "Canfield"
seasons: ["Spring"]
---

Hike to the peak with a sleeting on the way back down!