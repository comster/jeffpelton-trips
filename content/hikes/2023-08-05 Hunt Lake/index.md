---
title: "Backpacking to Hunt Lake"
date: 2023-08-05T00:00:00+00:00
images: ["images/IMG_1375.JPG"]
locations: "Hunt Lake"
seasons: ["Summer"]
---

Backpacking to overnight at Hunt Lake in the Cabinet Mountains Wilderness.
