---
title: "Pattee Canyon"
date: 2023-03-04T00:00:00+00:00
images: ["images/IMG_0736.jpg"]
locations: "Missoula"
seasons: ["Winter"]
---

Quick hike in the snowy canyon!
