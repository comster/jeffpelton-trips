---
title: "Grand Canyon Bright Angel Trail"
date: 2022-04-03T00:00:00+00:00
images: ["images/IMG_7022.jpg"]
locations: "Grand Canyon"
seasons: ["Spring"]
youtubes: ["sr37cRWzbGM"]
---

Hike down the Bright Angel Trail of the Grand Canyon!
