---
title: "Centennial Trail"
date: 2022-10-29T00:00:00+00:00
images: ["images/IMG_8238.jpg"]
locations: "Centennial Trail"
seasons: ["Fall"]
---

One of the last warm days of the year made for a great day to ride to Washington and back!  34 miles round trip in about 3.5 hours.
