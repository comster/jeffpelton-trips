---
title: "Higgens Point"
date: 2022-10-13T00:00:00+00:00
images: ["images/68740056662__1DDEAF68-63FA-407C-9246-7FB671E7BDCB.jpg"]
locations: "Centennial Trail"
seasons: ["Fall"]
youtubes: ["c-l1aBqH89o"]
---

Ride down Centennial Trail to Higgens Point